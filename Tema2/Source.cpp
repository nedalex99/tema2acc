#include <iostream>
#include <iostream>
#include <fstream>
#include <vector>
#include <queue>

void PrintMatrix(std::vector<std::vector<int>> matrix)
{
	for (int i = 1; i < matrix.size(); i++)
	{
		for (int j = 1; j < matrix[i].size(); j++)
		{
			std::cout << matrix[i][j] << " ";
		}
		std::cout << "\n";
	}
}

bool bfs(std::vector<std::vector<int>> rGraph, int s, int t, std::vector<int>& parent)
{
	int n = rGraph.size();
	std::vector<bool> visited(rGraph.size(), false);

	std::queue<int> q;
	q.push(s);
	visited[s] = true;
	parent[s] = -1;

	while (q.empty() == false)
	{
		auto u = q.front();
		q.pop();

		for (int i = 1; i < n; i++)
		{
			int v = i; 
			int capacity = rGraph[u][v];

			if (visited[v] == false && capacity > 0)
			{ 
				q.push(v);
				parent[v] = u;
				visited[v] = true;
			}
		}
	}

	if (visited[t] == true)
	{
		return true;
	}
	return false;
}

int FordFulkerson(std::vector<std::vector<int>> graph, int s, int t)
{
	int maxFlow = 0;
	
	std::vector<std::vector<int>> rGraph;
	int n = graph.size();
	for (int i = 0; i < n; i++)
	{
		std::vector<int> row;
		rGraph.push_back(row);
		for (int j = 0; j < n; j++)
		{
			rGraph[i].push_back(graph[i][j]);
		}
	}

	std::cout << "\n\n";
	PrintMatrix(rGraph);

	std::vector<int> parent;
	for (int i = 0; i < n; i++)
	{
		parent.push_back(-1);
	}

	while (bfs(rGraph, s, t, parent) == true)
	{
		int pathFlow = INT_MAX;

		int v = t;
		while (v != s)
		{
			int u = parent[v];

			int capacity = rGraph[u][v];
			pathFlow = std::min(pathFlow, capacity);

			v = u;
		}

		v = t;
		while (v != s)
		{
			int u = parent[v];

			rGraph[u][v] -= pathFlow;
			rGraph[v][u] += pathFlow;

			v = u;
		}
		maxFlow += pathFlow;
	}
	return maxFlow;
}

int main()
{
	std::fstream f("file.txt");

	int n, m;
	f >> n;
	f >> m;

	std::vector<std::vector<int>> matrix(n + 1, std::vector<int>(n + 1, 0));

	for (int i = 0; i < m; i++)
	{
		int x, y, z;
		f >> x;
		f >> y;
		f >> z;
		
		matrix[x][y] = z;
	}

	PrintMatrix(matrix);

	std::cout << "\n" << FordFulkerson(matrix, 1, n);
}